import {
  Box,
  Flex,
  Heading,
  Text,
  Grid,
  Center,
  Spinner,
  List,
  ListItem,
  Button,
  Spacer,
} from "@chakra-ui/react";

import { useQuery, gql } from "@apollo/client";
import { treasury } from "../../cardano/plutus/treasuryContract";
import { Data } from "@meshsdk/core";
import { GraphQLInlineDatum } from "../../types";
import { useEffect, useState } from "react";
import { Formik, Form, Field, FieldArray } from "formik";

// 2023-01-16: Drafting
// Next: refactor and extract key components

const TREASURY_DATUM_QUERY = gql`
  query DatumAtTreasury($treasuryAddress: String!) {
    utxos(where: { address: { _eq: $treasuryAddress } }) {
      txHash
      index
      value
      tokens {
        asset {
          assetId
          assetName
        }
        quantity
      }
      datum {
        bytes
        value
      }
    }
  }
`;

const createMeshTreasuryDatum = (hashes: Data): Data => {
  const resultingData: Data = {
    alternative: 0,
    fields: [hashes, treasury.issuerTokenName],
  };
  return resultingData;
};

const createProjectHashArray = (rawData: GraphQLInlineDatum): string[] => {
  const resultArray: string[] = [];
  rawData.value.fields[0].list.map((projectHash: { bytes: string }) => {
    resultArray.push(projectHash.bytes);
  });
  return resultArray;
};

// array.splice

// When is it time for a new data type - ie mapping hashes to project ids?
// This would require compiling a new Treasury Contract - when we next upgrade!

// export const treasuryDatum: Data = {
//     alternative: 0,
//     fields: [
//       [
//         "464a7376a01d7f8c58cf511285077e1b359fd6ff818f4375c9de1c2ec6fc1c2e",
//         "a05f79c041e4b2238b5301c15a8e1083e56f3600bb2483245e7b3905e0870a94",
//         "5a791293a0aaf6ffd9f7e9d0ced73916b8a3ad68fdae6c7afa8397b24dcb4b45",
//         "e0174b7bf4c06b975825c36170494732f50de8a4d9c21d272211ffe605875a44",
//         "34adf47f2ac91dff05eee13a4b96d584eb6730d1392b24d0684ad0bfba0e89c3",
//         "48170ea7183394b22c4437a08c7084b43ef794c11f6c5ad621f754fcb0adb6c2",
//         "6d9bd08690464a9d32bd9245620e55e97a6a6c5e7a00fab6dc97cabe96070ac2",
//         "9359ea25e967e9ba952c0b248a606d945657a14aeaa600eb37a140543ae4525d",
//         "a4d755863931395b53a6a2cdd6237239031ca99095f26715e0b05060ad3eb290",
//         "68b8d73a343590c26fc1773b69ccd4821436268fde33fc9810a29dc8688ffcfd",
//         "88f2f3c23311bd939537982245e7cf368b6be498a9ffc208d20d12724835cdb2",
//         "e3de3e3173e3fa886f9c175f78f77aefe87239f8a822b7647e4fe1d9f0113f3a",
//       ],
//       treasury.issuerTokenName,
//     ],
//   };

const ApprovedProjects = () => {
  const [currentTreasuryDatum, setCurrentTreasuryDatum] = useState<
    Data | undefined
  >(undefined);
  const [currentHashArray, setCurrentHashArray] = useState<string[]>([]);

  const treasuryDatumQuery = useQuery(TREASURY_DATUM_QUERY, {
    variables: {
      treasuryAddress: treasury.address,
    },
  });

  const error = treasuryDatumQuery.error;
  const loading = treasuryDatumQuery.loading;
  const data = treasuryDatumQuery.data;

  useEffect(() => {
    if (data) {
      setCurrentHashArray(createProjectHashArray(data.utxos[0].datum));
    }
  }, [data]);

  useEffect(() => {
    if (currentHashArray.length > 0) {
      setCurrentTreasuryDatum(createMeshTreasuryDatum(currentHashArray));
    }
  }, [currentHashArray]);

  if (loading) {
    return (
      <Center p="10">
        <Spinner size="xl" speed="1.0s" />
      </Center>
    );
  }

  if (error) {
    console.error(error);
    return <Heading size="lg">Error loading data...</Heading>;
  }

  return (
    <Box w="90%" mx="auto">
      <Heading py="5" size="4xl">
        Current List of Approved Projects
      </Heading>
      <Box p="5">
        <Heading>Nice List:</Heading>
        <Text>{JSON.stringify(data.utxos[0].datum.value.fields[0].list)}</Text>
      </Box>
      <Box p="5">
        <Heading>Process List as Mesh Datum:</Heading>
        <pre>
          <code className="language-js">
            {JSON.stringify(currentTreasuryDatum, null, 2)}
          </code>
        </pre>
      </Box>
      <Box p="5">
        <Heading>Add and Remove Project Hashes</Heading>
        <Text py="2">
          New Project: show Project associated with each Hash. Can be
          accomplished by Project IDs to Datum (more expensive) or by writing
          some new JS/TS functions in Front End.
        </Text>
        <Text py="2">
          Emergent Governance: What are the rules of updating this list?
        </Text>
        <Text py="2">Next Step: Add live Transaction to this page</Text>
        <Box>
          <Formik
            initialValues={{ hashes: currentHashArray }}
            enableReinitialize
            onSubmit={(values) => {
              setCurrentHashArray(values.hashes);
            }}
            render={({ values }) => (
              <Form>
                <FieldArray
                  name="hashes"
                  render={(arrayHelpers) => (
                    <Box p="5">
                      {values.hashes && values.hashes.length > 0 ? (
                        values.hashes.map((hash: string, index) => (
                          <Flex
                            direction="row"
                            key={index}
                            bg="gray.700"
                            color="purple.200"
                            my="2"
                            p="2"
                            alignItems="center"
                            w="80%"
                          >
                            {hash ? (
                              <code>{hash}</code>
                            ) : (
                              <Box>
                                <Field
                                  name={`hashes.${index}`}
                                  placeholder="paste new hash"
                                />
                              </Box>
                            )}
                            <Spacer />
                            <Button
                              type="button"
                              mx="2"
                              colorScheme="red"
                              onClick={() => arrayHelpers.remove(index)} // remove a friend from the list
                            >
                              -
                            </Button>
                            <Button
                              type="button"
                              mx="2"
                              colorScheme="green"
                              onClick={() => arrayHelpers.insert(index, "")} // insert an empty string at a position
                            >
                              +
                            </Button>
                          </Flex>
                        ))
                      ) : (
                        <Button
                          type="button"
                          onClick={() => arrayHelpers.push("")}
                        >
                          Add a Hash
                        </Button>
                      )}
                      <Box my="3">
                        <Button type="submit" colorScheme="purple">
                          Update Treasury Datum
                        </Button>
                      </Box>
                    </Box>
                  )}
                />
              </Form>
            )}
          />
        </Box>
      </Box>
      <Box p="5">
        <Heading>List of Outbound Project Hashes:</Heading>
        <List>
          {currentHashArray.map((projectHash: string, i) => (
            <ListItem key={i}>
              <code>{projectHash}</code>
            </ListItem>
          ))}
        </List>
      </Box>
    </Box>
  );
};

export default ApprovedProjects;
