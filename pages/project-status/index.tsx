import { ApolloQueryResult, gql, useQuery } from "@apollo/client";
import client from "../../apollo-client";
import {
  Box,
  Button,
  Center,
  Flex,
  Heading,
  Input,
  Link,
  NumberDecrementStepper,
  NumberIncrementStepper,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  Select,
  Spacer,
  Spinner,
  Table,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tooltip,
  Tr,
  chakra,
  color,
} from "@chakra-ui/react";
import { GetStaticProps } from "next";
import React, { useEffect, useState } from "react";
import { getAllEscrowTransactions } from "../../project-lib/utils";
import { escrow } from "../../cardano/plutus/escrowContract";
import { treasury } from "../../cardano/plutus/treasuryContract";
import {
  ExpandedState,
  FilterFn,
  SortingState,
  createColumnHelper,
  flexRender,
  getCoreRowModel,
  getExpandedRowModel,
  getPaginationRowModel,
  getSortedRowModel,
  useReactTable,
} from "@tanstack/react-table";
import {
  ArrowDownIcon,
  ArrowForwardIcon,
  MinusIcon,
  TriangleDownIcon,
  TriangleUpIcon,
} from "@chakra-ui/icons";
import { getAllProjectIds, getProjectData } from "../../project-lib/projects";
import { Project } from "../../types";

const ESCROW_QUERY = gql`
  query TransactionsWithMetadataKey($metadatakey: String!, $address: String!) {
    transactions(
      where: {
        _and: [
          { metadata: { key: { _eq: $metadatakey } } }
          {
            _or: [
              { outputs: { address: { _eq: $address } } }
              { inputs: { address: { _eq: $address } } }
            ]
          }
        ]
      }
    ) {
      hash
      includedAt
      metadata {
        key
        value
      }
      inputs {
        txHash
        sourceTxIndex
        address
        value
        tokens {
          asset {
            assetId
            assetName
          }
          quantity
        }
      }
      outputs {
        txHash
        index
        address
        value
        tokens {
          asset {
            assetId
            assetName
          }
          quantity
        }
      }
    }
  }
`;

type ProjectStatusResult = {
  id: string;
  title: string;
  expiration: number | null;
  contributorToken: string;
  multipleCommitments: boolean | null;
  subRows: ProjectStatusResult[];
  commitment: number | string;
  distribution: number | string;
  status: string;
};

// Get all the projects in a property
export const getStaticProps: GetStaticProps = async (context) => {
  const projects = await Promise.all(
    getAllProjectIds().map((pId) => {
      return getProjectData(pId.params.id) as Promise<Project>;
    })
  );

  return {
    props: { projects },
  };
};

const columnHelper = createColumnHelper<ProjectStatusResult>();

const columns = [
  columnHelper.accessor("id", {
    cell: ({ row, getValue }) => (
      <Text whiteSpace="nowrap">
        {row.getCanExpand() ? (
          <button
            {...{
              onClick: row.getToggleExpandedHandler(),
              style: { cursor: "pointer" },
            }}
          >
            {row.getIsExpanded() ? (
              <ArrowDownIcon color="yellow.400" boxSize="1.25em" />
            ) : (
              <ArrowForwardIcon color="yellow.400" boxSize="1.25em" />
            )}
          </button>
        ) : (
          <MinusIcon color="yellow.400" boxSize="1.25em" />
        )}{" "}
        <Link href={`/projects/${getValue()}`} color={"white"}>
          {getValue()}
        </Link>
      </Text>
    ),
    header: "Project",
  }),
  columnHelper.accessor("title", {
    cell: (info) => info.getValue(),
    header: "Title",
  }),
  columnHelper.accessor("contributorToken", {
    cell: (info) => info.getValue(),
    header: "Contributor Token",
  }),
  columnHelper.accessor("expiration", {
    cell: (info) =>
      info.getValue() ? (
        <Tooltip label={new Date(info.getValue() as number).toLocaleString()}>
          {new Date(info.getValue() as number).toLocaleDateString()}
        </Tooltip>
      ) : (
        ""
      ),
    header: "Expiration Time",
  }),
  columnHelper.accessor("multipleCommitments", {
    cell: (info) =>
      info.getValue() === null ? "" : info.getValue() ? "Yes" : "No",
    header: "Multiple Commitments",
  }),
  columnHelper.accessor("commitment", {
    cell: (info) => {
      if (typeof info.getValue() === "number" || info.getValue() === "") {
        return info.getValue();
      } else {
        return (
          <Tooltip
            label={new Date(
              Date.parse(info.getValue() as string)
            ).toLocaleString()}
          >
            {new Date(
              Date.parse(info.getValue() as string)
            ).toLocaleDateString()}
          </Tooltip>
        );
      }
    },
    header: "Commitment",
  }),
  columnHelper.accessor("distribution", {
    cell: (info) => {
      if (typeof info.getValue() === "number" || info.getValue() === "") {
        return info.getValue();
      } else {
        return (
          <Tooltip
            label={new Date(
              Date.parse(info.getValue() as string)
            ).toLocaleString()}
          >
            {new Date(
              Date.parse(info.getValue() as string)
            ).toLocaleDateString()}
          </Tooltip>
        );
      }
    },
    header: "Distribution",
  }),
  columnHelper.accessor("status", {
    header: "Status",
    cell: (info) => (
      <Text color={info.getValue() === "Open" ? "green.400" : "red.400"}>
        {info.getValue()}
      </Text>
    ),
  }),
];

const ProjectStatusPage: React.FC<{ projects: Project[] }> = ({ projects }) => {
  const [sorting, setSorting] = useState<SortingState>([]);
  const [tableData, setTableData] = useState<ProjectStatusResult[]>([]);
  const [expanded, setExpanded] = useState<ExpandedState>({});

  const table = useReactTable({
    columns,
    data: tableData,
    getSubRows: (row) => row.subRows,
    onExpandedChange: setExpanded,
    getCoreRowModel: getCoreRowModel(),
    onSortingChange: setSorting,
    getSortedRowModel: getSortedRowModel(),
    getExpandedRowModel: getExpandedRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
    initialState: {
      pagination: {
        pageSize: 20
      }
    },
    state: {
      sorting,
      expanded
    }
  });

  const { data, loading, error } = useQuery(ESCROW_QUERY, {
    variables: {
      metadatakey: treasury.metadataKey,
      address: escrow.address,
    },
  });

  useEffect(() => {
    if (!data) {
      return;
    }

    const results = getAllEscrowTransactions(data);

    const projectStatusResult: ProjectStatusResult[] = [];

    projects.forEach((project) => {
      projectStatusResult.push({
        id: project.id,
        title: project.title,
        contributorToken: "",
        expiration: null,
        multipleCommitments: project.multipleCommitments,
        subRows: [],
        commitment: 0,
        distribution: 0,
        status: "Open",
      });
    });

    results.forEach(async (escrowTx) => {
      var projectStatus = projectStatusResult.find(
        (projectResult) => escrowTx.metadata.id === projectResult.id
      );

      if (projectStatus) {
        var subRow = projectStatus.subRows.find(
          (contribution) =>
            contribution.contributorToken === escrowTx.contributorTokenName &&
            contribution.expiration == escrowTx.metadata.expTime
        )!;

        if (!subRow) {
          subRow = {
            id: "",
            title: "",
            contributorToken: escrowTx.contributorTokenName ?? "NA",
            multipleCommitments: null,
            expiration: escrowTx.metadata.expTime,
            commitment: "",
            distribution: "",
            subRows: [],
            status: "",
          };
          projectStatus.subRows.push(subRow);
        }

        switch (escrowTx.metadata.txType) {
          case "Commitment":
            subRow.commitment = escrowTx.includedAt as string;
            projectStatus.commitment = (projectStatus.commitment as number) + 1;
            break;
          case "Distribute":
            subRow.distribution = escrowTx.includedAt;
            projectStatus.distribution =
              (projectStatus.distribution as number) + 1;
            break;
        }
      }
    });

    // Calculate status

    projectStatusResult.map((result) => {
      const isOpen =
        !result.subRows.every((subRow) => subRow.distribution) ||
        result.commitment === 0;

      result.subRows.forEach((contribution) => {
        contribution.status =
          contribution.distribution !== "" ? "Closed" : "Open";
      });

      result.status = isOpen
        ? "Open"
        : result.multipleCommitments
        ? "Open"
        : "Closed";
    });

    setTableData(projectStatusResult);
  }, [data, projects]);

  if (loading) {
    return (
      <Center p="10">
        <Spinner size="xl" speed="1.0s" />
      </Center>
    );
  }

  if (error) {
    console.error(error);
    return <Heading size="lg">Error loading data...</Heading>;
  }

  return (
    <Box w="90%" mx="auto" overflowY="auto">
      <Heading size="4xl" mb="4">
        Project Status
      </Heading>
      <Table size={"sm"}>
        <Thead>
          {table.getHeaderGroups().map((headerGroup) => (
            <Tr key={headerGroup.id}>
              {headerGroup.headers.map((header) => {
                // see https://tanstack.com/table/v8/docs/api/core/column-def#meta to type this correctly
                const meta: any = header.column.columnDef.meta;
                return (
                  <Th
                    key={header.id}
                    onClick={header.column.getToggleSortingHandler()}
                    isNumeric={meta?.isNumeric}
                  >
                    {flexRender(
                      header.column.columnDef.header,
                      header.getContext()
                    )}

                    <chakra.span pl="4">
                      {header.column.getIsSorted() ? (
                        header.column.getIsSorted() === "desc" ? (
                          <TriangleDownIcon aria-label="sorted descending" />
                        ) : (
                          <TriangleUpIcon aria-label="sorted ascending" />
                        )
                      ) : null}
                    </chakra.span>
                  </Th>
                );
              })}
            </Tr>
          ))}
        </Thead>
        <Tbody>
          {table.getRowModel().rows.map((row) => (
            <Tr key={row.id}>
              {row.getVisibleCells().map((cell) => {
                // see https://tanstack.com/table/v8/docs/api/core/column-def#meta to type this correctly
                const meta: any = cell.column.columnDef.meta;
                return (
                  <Td key={cell.id} isNumeric={meta?.isNumeric}>
                    {flexRender(cell.column.columnDef.cell, cell.getContext())}
                  </Td>
                );
              })}
            </Tr>
          ))}
        </Tbody>
      </Table>
      <Flex mt="2" alignItems="center">
        <Button
          onClick={() => table.setPageIndex(0)}
          disabled={!table.getCanPreviousPage()}
          me="2"
          color="yellow.400"
          variant="outline"
        >
          {"<<"}
        </Button>
        <Button
          onClick={() => table.previousPage()}
          disabled={!table.getCanPreviousPage()}
          me="2"
          color="yellow.400"
          variant="outline"
        >
          {"<"}
        </Button>
        <Button
          onClick={() => table.nextPage()}
          disabled={!table.getCanNextPage()}
          me="2"
          color="yellow.400"
          variant="outline"
        >
          {">"}
        </Button>
        <Button
          onClick={() => table.setPageIndex(table.getPageCount() - 1)}
          disabled={!table.getCanNextPage()}
          me="2"
          color="yellow.400"
          variant="outline"
        >
          {">>"}
        </Button>
        <Box whiteSpace="nowrap" me="2">
          Page
          <strong>
            {table.getState().pagination.pageIndex + 1} of{" "}
            {table.getPageCount()}
          </strong>
        </Box>
        <Box whiteSpace="nowrap" me="2">
          <Flex alignItems="center">
            <Box me="2">| Go to page:</Box>
            <NumberInput
              defaultValue={table.getState().pagination.pageIndex + 1}
              onChange={(value) => {
                const page = value ? Number(value) - 1 : 0;
                table.setPageIndex(page);
              }}
              width="fit-content"
            >
              <NumberInputField />
              <NumberInputStepper>
                <NumberIncrementStepper color="yellow.400" />
                <NumberDecrementStepper color="yellow.400" />
              </NumberInputStepper>
            </NumberInput>
          </Flex>
        </Box>
        <Spacer />
        <Select
          value={table.getState().pagination.pageSize}
          onChange={(e) => {
            table.setPageSize(Number(e.target.value));
          }}
          width="fit-content"
          me="2"
        >
          {[20, 30, 40, 50].map((pageSize) => (
            <option
              style={{
                backgroundColor: "var(--chakra-colors-chakra-body-text)",
              }}
              key={pageSize}
              value={pageSize}
            >
              Show {pageSize}
            </option>
          ))}
        </Select>
      </Flex>
    </Box>
  );
};

export default ProjectStatusPage;
