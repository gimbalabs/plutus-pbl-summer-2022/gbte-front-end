---
id: "0015"
datePosted: "2022-09-29"
title: "Create Project Status Overview"
lovelace: 100000000
gimbals: 2000
status: "Open"
devCategory: "Data"
bbk: [""]
approvalProcess: 4
multipleCommitments: false
repositoryLink: "https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-front-end"
---

## Outcome:
- Create an ***effective*** way to view the status of Project Commitments.

## Requirements:
Note: The ***effectiveness*** of a data view is subjective. See "How to Complete" for details about the approval process.
1. Your view component should show all Projects that are currently "open". We can define as open Project as one with a locked UTxO at the Escrow Contract Address, representing a Project Commitment that has not yet been Distributed.
2. Your view component should also show all Projects that have been Distributed.

## How To Start:
- Your view component can use the raw data presented in [`/pages/dashboard`](/dashboard).
- Review the Data tracking components in `/components/tracking/`.
- Think about the story this data view is telling. Who are you telling it to?

## Links + Tips:
- When you work on this task, you will discover new challenges to Javascript and React development. Plan on bringing your questions to the rest of the team at Live Coding, or by asking for help in Discord.
- If you need to define new Types, you can extend any of the existing types in `/types/index.ts`. Read the [TypeScript documentation on Types to learn more](https://www.typescriptlang.org/docs/handbook/2/objects.html).
- CharkaUI has [some pretty good Data Display components](https://chakra-ui.com/docs/components/badge), [like Table](https://chakra-ui.com/docs/components/table), but feel free to use [other](https://mui.com/x/react-data-grid/) [libraries](https://flowbite.com/docs/components/tables/) if you think they are helpful.

## How To Complete:
- Demonstrate your results at Live Coding, take feedback, and we will test the [consent decision-making process](https://www.sociocracyforall.org/consent-decision-making/).
- Submit a Merge Request to [GPTE-front-end](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-front-end)