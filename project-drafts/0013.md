---
id: "0013"
datePosted: "2022-09-29"
title: "Extend GPTE to Handle multiple Native Tokens"
lovelace: 250000000
gimbals: 5000
status: "Coming Soon"
devCategory: "Plutus"
bbk: [""]
approvalProcess: 4
multipleCommitments: false
repositoryLink: "https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-plutus"
---

## Outcome:

## Requirements:

## How To Start:

## Links + Tips:

## How To Complete:
